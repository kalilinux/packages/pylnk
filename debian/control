Source: pylnk
Section: python
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-setuptools
Standards-Version: 4.6.0
Homepage: https://github.com/strayge/pylnk
Vcs-Browser: https://gitlab.com/kalilinux/packages/pylnk
Vcs-Git: https://gitlab.com/kalilinux/packages/pylnk.git
Testsuite: autopkgtest-pkg-python

Package: python3-pylnk3
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Breaks: python3-pylnk (<< 0.4.2-0kali2)
Replaces: python3-pylnk (<< 0.4.2-0kali2)
Description: Python library for reading and writing Windows shortcut files (.lnk)
 This package contains a Python library for reading and writing Windows
 shortcut files (.lnk).
 .
 This library can parse .lnk files and extract all relevant information from
 them. Parsing a .lnk file yields a LNK object which can be altered and saved
 again. Moreover, .lnk file can be created from scratch be creating a LNK
 object, populating it with data and then saving it to a file. As that process
 requires some knowledge about the internals of .lnk files, some convenience
 functions are provided.
 .
 Limitation: Windows knows lots of different types of shortcuts which all have
 different formats. This library currently only supports shortcuts to files and
 folders on the local machine.
 .
 This package installs the library for Python 3.

Package: python3-pylnk
Architecture: all
Section: oldlibs
Depends: ${misc:Depends}, python3-pylnk3
Description: transitional package
 This is a transitional package. It can safely be removed.
